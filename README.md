# cgi

## Official documentation
* [*The Common Gateway Interface (CGI) Version 1.1*
  ](https://www.ietf.org/rfc/rfc3875) RFC 3875 2004-10

## Unofficial documentation
* [*Common Gateway Interface*
  ](https://en.wikipedia.org/wiki/Common_Gateway_Interface)
* [cgi environment variables](https://google.com/search?q=cgi+environment+variables)
* (fr) [*Variables d'environnement CGI*
  ](https://fr.wikipedia.org/wiki/Variables_d%27environnement_CGI)
